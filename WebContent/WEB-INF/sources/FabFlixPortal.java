
/* A servlet to display the contents of the MySQL movieDB database */

import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class FabFlixPortal extends HttpServlet
{
    public String getServletInfo()
    {
       return "Servlet connects to MySQL database and displays result of a SELECT";
    }

    // Use http GET : information that you put in is placed in the header part of the http packet
    //   If you use http POST, information that you put in is placed in the data part of the http packet

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        String loginUser = "mavian";
        String loginPasswd = "letmein123";
        String loginUrl = "jdbc:mysql://localhost:3306/moviedb";

        response.setContentType("text/html");    // Response mime type

        // Output stream to STDOUT
        PrintWriter out = response.getWriter();

        try
           {
              //Class.forName("org.gjt.mm.mysql.Driver");
              Class.forName("com.mysql.jdbc.Driver").newInstance();

              Connection dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
              // Declare our statement
              Statement statement = dbcon.createStatement();

              String email = request.getParameter("username");
              String password = request.getParameter("password");
              String query = "SELECT password, first_name FROM customers WHERE email='" + email + "'";

              // Perform the query
              ResultSet rs = statement.executeQuery(query);
              
              if(rs.next()){

            	  if(rs.getString(1).equals(password)){
            		  out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">" +
            		  			  "<HTML>"
            		  			  + "<HEAD>"
            		  			  + "<style>"
            		  			  + "	button:hover{background-color:lightblue; font-size:1.1em}"
            		  			  + "</style>"
            		  			  + "<TITLE>FabFlix - Watch Movies Any Time, Anywhere.</TITLE>"
            		  			  + "</HEAD>");
            		  out.println("<BODY>");
            		  out.println("<div style=\"background-color:#C60000; color:#FFFFFF\">" +
            				  	  	"<h1>&nbsp;FabFlix</h1>" + 
            				  	  "</div>");
            		  out.println("<div align=\"right\">"
            		  			+ "		<table>"
            		  			+ "			<tr>"
            		  			+ "				<td> <a href='../home.html' target='main'>Home</a> </td>"
            		  			+ "				<td> <a href='../shoppingcart.html' target='main'>My Shopping Cart</a> </td>"
            		  			+ "				<td> <a href='../index.html'>Sign out</a> </td>"
            		  			+ "			</tr>"
            		  			+ "		</table>"
            		  			+ "</div>");            		  
            		  out.println("<div> <p> &nbspWelcome, <span style=\"font-size:24px\"> <strong>" + rs.getString(2) + "!</strong></span> </p> </div>");
            		  
            		  out.println("<div align=\"center\">");
            		  out.println("<table style=\"width:400px\">"
		            		  		+ "<tr>"
		            		  		+ "		<td colspan=3 align=\"center\">"
		            		  		+ "			<h3> What do you wanna do today? </h3>"
		            		  		+ "		</td>"
		            		  		+ "</tr>"
		            		  		+ "<tr>"
		            		  		+ "		<td>"
		            		  		+ "			<form action=\"../search.html\" target='main'>"
		            		  		+ "			<button style=\"height:150px; width:150px\">Search for a movie</button>"
		            		  		+ "			</form>"		
		            		  		+ "		</td>"
		            		  		+ "		<td style=\"width:100px\"> &nbsp </td>"
		            		  		+ "		<td>"
		            		  		+ "			<form action=\"../browse.html\" target='main'>"
		            		  		+ "			<button style=\"height:150px; width:150px\">Browse for movies</button>"
		            		  		+ "			</form>"
		            		  		+ "		</td>"
		            		  		+ "</tr>"
		            		  	+ "</table>");
            		  out.println("</div>");
            		  
            		  out.println("<iframe src=\"../home.html\" name=\"main\" scrolling=\"auto\" style=\"border-style:none; width:100%\"></iframe>");
            		  
                      out.println("</BODY> </HTML>");
            	  } 
            	  else{
            		  goToIndex(out);
            	  }
              }
              else{
            	  goToIndex(out);
              }

              rs.close();
              statement.close();
              dbcon.close();
            }
        catch (SQLException ex) {
              while (ex != null) {
                    System.out.println ("SQL Exception:  " + ex.getMessage ());
                    ex = ex.getNextException ();
                }  // end while
            }  // end catch SQLException

        catch(java.lang.Exception ex)
            {
                out.println("<HTML>" +
                            "<HEAD><TITLE>" +
                            "MovieDB: Error" +
                            "</TITLE></HEAD>\n<BODY>" +
                            "<P>SQL error in doGet: " +
                            ex.getMessage() + "</P></BODY></HTML>");
                return;
            }
         out.close();
    }
    
    private void goToIndex(PrintWriter out)
    {
    	out.println("Error Logging in.");
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException
    {
    	doGet(request, response);
    }
}